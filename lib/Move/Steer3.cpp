#include "Steer3.h"
#include <Arduino.h>
//#include <BluetoothSerial.h>

Steer3::Steer3(MD **md_):MoveBase(md_){}
/*
md[0]~md[2] : Angle
md[3]~md[5] : Speed
*/
//V_x[m/s], V_y[m/s], V_angular[rad/s]
void Steer3::Move(double V_x, double V_y, double V_angular){
    double VY,VX,angle;
    double dX=0.0, dY=0.0;
    double deltaX=0.0, deltaY=0.0, deltaAngle=0.0;
    int i;
    //printf("V_x %.2lf V_y %.2lf ", V_x, V_y);
    V_angular *= body_r / wheel_r / PI / 2.0;
    V_x/=2.0*PI*wheel_r;
    V_y/=2.0*PI*wheel_r;
    for(i = 0; i < 3; i++){
        VY = V_y + V_angular*sin_[i];   VY+=10E-200;
        VX = V_x + V_angular*cos_[i];
        angle = atan2(VY, VX) + ((VY>0.0) ? 0.0:PI);
        md[i]->Move(angle);
        md[i+3]->Move(hypot(VY,VX)*((VY>0.0) ? 1.0:-1.0));
        printf("M[%d] %lf[deg] %lf[rps] ", i, md[i]->now_val/PI*180.0, md[i+3]->now_val/PI/2.0/50.0E-3);       
        dX = md[i+3]->now_val * cos(md[i]->now_val);
        dY = md[i+3]->now_val * sin(md[i]->now_val);
        deltaAngle += dX * cos_[i] + dY * sin_[i];
        deltaX+=dX;
        deltaY+=dY;
    }
    Angle += deltaAngle * wheel_r / 3.0 / body_r;
    deltaX *= wheel_r / 3.0;
    deltaY *= wheel_r / 3.0;
    X += deltaX*cos(Angle) - deltaY*sin(Angle);
    Y += deltaX*sin(Angle) + deltaY*cos(Angle);
    printf("%lf ", hypot(deltaX,deltaY)/50.0E-3);
   printf("X=%4lf Y=%4lf Angle=%4lf\n", X, Y, Angle/PI*180);
}