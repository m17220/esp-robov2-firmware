#ifndef __MOVEBASE_H__
#define __MOVEBASE_H__
#include "MD.h"
#define _USE_MATH_DEFINES
#include <math.h>

#define pow2(x) ((x)*(x))

class MoveBase{
    public:    
    MD **md;
    MoveBase();
    MoveBase(MD **md_);
    virtual ~MoveBase();
    
    //Coordinate X[m]   Y[m]    Angle[rad]
    double X, Y, Angle;

    virtual void Move(double V_x, double V_y, double V_angular);
    virtual void Stop();

    void SetSize(double wheel, double body);
    void MoveXY(double V_x, double V_y);
    void MoveR(double V_r, double arg);
    void MoveR(double V_r, double arg, double V_angular);
    void Rotate(double V_anglular);

    void SetGain(double P_XY_, double P_Angle_);
    double MoveTo(double TargetX, double TargetY, double TargetAngle);

    protected:
    //タイヤの半径[m]
    double wheel_r;
    //足回りの半径[m]
    double body_r;

    private:
    //P Gain
    double P_XY, P_Angle;
    //I Gain
    double I_XY, I_Angle;
    double Sum_deltaX, Sum_deltaY, Sum_deltaAngle;
};

#endif