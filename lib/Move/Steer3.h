#ifndef __STEER3_H__
#define __STEER3_H__

#include "MoveBase.h"
#include "MD.h"
#define M2_SQRT3 0.86602540378443864676372317075294
        //            M0    M1    M2
const double cos_[3]={-1.0, 0.5, 0.5};
const double sin_[3]={0.0, -M2_SQRT3, M2_SQRT3};

class Steer3 : public MoveBase{
    public:
    Steer3(MD **md_);
    void Move(double V_x, double V_y, double V_angular);
};
#endif
/*
           <---m[0]---

     \                   ∧
      \                 /
       m[1]           m[2]
        \             /
         v           /
*/