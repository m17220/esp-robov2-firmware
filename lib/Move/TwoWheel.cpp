#include "MoveBase.h"
#include "TwoWheel.h"
#include <Arduino.h>
TwoWheel::TwoWheel(MD **md_):MoveBase(md_){}

void TwoWheel::Move(double V, double V_angular){
    md[0]->Move(V - V_angular);
    md[1]->Move(V + V_angular);
}

void TwoWheel::Move(double V_x, double V_y, double V_angular){
    Move(V_x, V_angular);
}