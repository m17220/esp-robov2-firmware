#include "MoveBase.h"

MoveBase::MoveBase(){
}

MoveBase::MoveBase(MD **md_){
    md = md_;
    X=Y=Angle=0.0;
}

MoveBase::~MoveBase(){
    //delete[] md;
}

void MoveBase::Move(double V_x, double V_y, double V_angular){}

void MoveBase::Stop(){
    Move(0.0, 0.0, 0.0);
}

void MoveBase::SetSize(double wheel, double body){
    wheel_r = wheel;
    body_r = body;
}

void MoveBase::MoveXY(double V_x, double V_y){
    Move(V_x, V_y, 0);
}

void MoveBase::MoveR(double V_r, double arg, double V_angular){
    double V_x, V_y;
    V_x = V_r * cos(arg);
    V_y = V_r * sin(arg);
    Move(V_x, V_y, V_angular);
}

void MoveBase::MoveR(double V_r, double arg){
    MoveR(V_r, arg, 0);
}

void MoveBase::Rotate(double V_angular){
    Move(0, 0, V_angular);
}

void MoveBase::SetGain(double P_XY_, double P_Angle_){
    P_XY = P_XY_;
    P_Angle = P_Angle_;
}

double MoveBase::MoveTo(double TargetX, double TargetY, double TargetAngle){
    double dX,dY,dAngle;
    //目標値との差をとる
    dX = X-TargetX;
    dY = Y-TargetY;
    dAngle = Angle-TargetAngle;
    //誤差の累積値
    Sum_deltaX+=dX;
    Sum_deltaY+=dY;
    Sum_deltaAngle+=dAngle;
    Move(dX*P_XY+Sum_deltaX*I_XY, dY*P_XY+Sum_deltaY*I_XY, dAngle*P_Angle+Sum_deltaAngle*I_Angle);  
    return sqrt(pow2(dX)+pow2(dY)+pow2(dAngle));    //目標値とどれくらい離れているか
}