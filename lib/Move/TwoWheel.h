#ifndef __TWO_WHEEL_H__
#define __TWO_WHEEL_H__

#include "MoveBase.h"
#include "MD.h"

class TwoWheel : public MoveBase{
    public:
    TwoWheel(MD **md_);
    void Move(double V, double V_angular);
    void Move(double V_x, double V_y, double V_angular);
};

#endif