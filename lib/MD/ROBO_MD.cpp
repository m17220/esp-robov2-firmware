#include <Arduino.h>
#include "ROBO_MD.h"

roboMD::roboMD(int num){
    mdNum = num;
    speedRate = 1.0;
}

void roboMD::Move(double val){    
    val *= speedRate;
    union{
        //for transmit
        uint8_t bit8[2];
        //instruction
        struct { 
            uint8_t padding : 3;
            uint8_t mdNum : 2;
            uint8_t ph : 1;
            uint16_t pwmDuty :10;
        }__packed ir;
    }data;
    data.ir.padding  = 0x00;

    if(val >= 1.0)
        val = 1.0;
    
    if(val <= -1.0)
        val = -1.0;
    
    if(val >= 0.0)
        data.ir.ph = 1;
    else
        data.ir.ph = 0;

    data.ir.pwmDuty  = (uint16_t)(abs(val)*PIC_PWM_RESOLUTION);
    data.ir.mdNum = this->mdNum;

    //printf("md%d duty:%d %x %x\n", data.ir.mdNum, data.ir.pwmDuty, data.bit8[0], data.bit8[1]);
    
    //transmit
    Wire.beginTransmission(PIC_ADDRESS);
    Wire.write(data.bit8[0]);
    Wire.write(data.bit8[1]);
    Wire.endTransmission();
    
}
