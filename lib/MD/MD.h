#ifndef __MD_H__
#define __MD_H__

#define Gear    (14.0/35.0)//(8.0 / 15.0)    //wheel / encorder

class MD{
    public:

    //Speed:[rps] or [rad]   Angle:[rad]   
    double now_val;

    virtual void Move(double val) = 0;
    virtual void Brake();

    void Reverse(bool rev);  

    protected: 
    bool reverse = false;   
};

#endif