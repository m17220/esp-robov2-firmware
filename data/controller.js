let width, height;


/*
Data format

| cmd | EN | x | y | TRIANGLE | RECTANGLE | CIRCLE | CROSS | 

controller cmd = 0x01

*/
const CONTROLLER_CMD = 0x01;
let controller = {x:0x00, y:0x00, EN:0x00, TRIANGLE:0x00, RECTANGLE:0x00, CIRCLE:0x00, CROSS:0x00};
//Websocket
let ws = new WebSocket("ws:/"+window.location.hostname+"/ws");
ws.binaryType = "arraybuffer";
let intervalId;
ws.onopen = function(){
    console.log("open");
    intervalId = setInterval(sendData, 100);
}

ws.onclose = function(){
    clearInterval(intervalId);
    console.log("close");
}

ws.onmessage = function(e){
    let buf = new Int8Array(e.data);

    if(buf[0] == 0x02){
        //Battery
        console.log(buf[1], buf[2]);
        logicBattery.Value = buf[1];
        motorBattery.Value = buf[2];
    }
}

function sendData(){
    
    let buf = new Uint8Array(8);
    buf[0] = CONTROLLER_CMD;
    buf[1] = controller.EN;
    buf[2] = controller.x;
    buf[3] = controller.y;
    buf[4] = controller.TRIANGLE;
    buf[5] = controller.RECTANGLE;
    buf[6] = controller.CIRCLE;
    buf[7] = controller.CROSS;
    ws.send(buf);
}


//setting json  (XMLHttpRequest)
let loadSettingFromESP32 = () => {
    let xhr = new XMLHttpRequest();
    
    xhr.responseType = "json";

    xhr.onreadystatechange = ()=>{
        console.log(xhr.readyState);
        console.log(xhr.status);
        if(xhr.readyState == 4 && xhr.status == 200){
            setting_json = xhr.response;
            applySettingToUI(setting_json);
        }
    }
    
    xhr.open("GET", "/setting.json");
    
    xhr.send(null);
}

let uploadSettingFromBrowser = (setting_string) => {
    console.log("upload setting from browser", setting_string);

    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = () => {
        console.log(xhr.readyState);
        console.log(xhr.status);
    }
    xhr.open("POST", "/setting_upload.json");

    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.send(setting_string);
    
};

//Joystick
let joystick = document.getElementById("joystick");
let stick = joystick.getBoundingClientRect();
let wrap = document.getElementById("joywrap").getBoundingClientRect();
let stick_r = window.innerWidth*0.15/2;

let setJoystickCoordinate = (x, y) => {
    joystick.style.left = String(x + stick_r) + "px";
    joystick.style.top = String(y + stick_r) + "px";
}
window.onresize = () =>{
    console.log("onresize");
    stick_r = window.innerWidth*0.15/2;
    setJoystickCoordinate(0, 0);
};

let moveJoystick = (e) => {
    function slice2(j){
        //for cut "px" and transform to number
        return Number(j.slice(0, -2));
    }

    let x,y;
    function calcXY(){
        x = slice2(joystick.style.left) - stick_r;
        y = slice2(joystick.style.top) - stick_r;
    }

    calcXY();
    x += e.movementX;
    y += e.movementY;
    let r = Math.hypot(x, y);
    let limit = stick_r;
    let theta = Math.atan2(y, x);
    if(r > limit){
        //overhang
        x = limit * Math.cos(theta);
        y = limit * Math.sin(theta);
    }

    setJoystickCoordinate(x, y);
    
    //Normalize
    x /= limit;
    y /= limit;
    controller.x = x * 127;
    controller.y = y * 127;
    console.log(controller.x, controller.y);

}

let detouchJoystick = () => {
    setJoystickCoordinate(0, 0);
    window.removeEventListener("mousemove", moveJoystick);
    window.removeEventListener("mouseup", detouchJoystick);
    console.log("detouch");
    controller.x = 0;
    controller.y = 0;
}

let touchJoystick = (e) => {
    setJoystickCoordinate(0, 0);
    window.addEventListener("mousemove", moveJoystick);
    window.addEventListener("mouseup", detouchJoystick);
    console.log("touch");
}
let touchX, touchY;
joystick.addEventListener("mousedown", touchJoystick);
joystick.addEventListener("touchstart", (e)=>{
    e.preventDefault();
    setJoystickCoordinate(0, 0);
    let ev = e.targetTouches[0];
    touchX = ev.pageX;
    touchY = ev.pageY;
}, false);
joystick.addEventListener("touchmove", (evt)=>{
    evt.preventDefault();
    let e = {movementX:0, movementY:0};
    let ev = evt.targetTouches[0];
    e.movementX = ev.pageX - touchX;
    e.movementY = ev.pageY - touchY;
    console.log(e);
    touchX = ev.pageX;
    touchY = ev.pageY;
    moveJoystick(e);
});
joystick.addEventListener("touchend", ()=>{
    setJoystickCoordinate(0, 0);
    touchX = 0;
    touchY = 0;
    detouchJoystick();
});
joystick.addEventListener("touchcancel", ()=>{
    setJoystickCoordinate(0, 0);
    touchX = 0;
    touchY = 0;
    detouchJoystick();
});
//----------------------------------------------------------------

//ENABLE_SW
let en_sw = document.getElementById("enable_sw");
en_sw.onclick = () => {
    if(en_sw.value == "DISABLED"){
        en_sw.value = "ENABLED";
        en_sw.style.backgroundColor = "green";
        controller.EN = 0x01;
    }
    else{
        en_sw.value = "DISABLED";
        en_sw.style.backgroundColor = "red";
        controller.EN = 0x00;
    }
}

//------------------------------------------------------------------

//Button
let buttons = [...document.getElementsByClassName("arm_button")];
console.log(buttons);
//off
let buttonUp = (e) => {
    let b = e.target;
    console.log(b.id);
    console.log("up");
    controller[b.id] = 0x00;
}

//on
let buttonDown = (e) => {
    let b = e.target;
    console.log(b.id);
    console.log("down");
    controller[b.id] = 0xff;
}

buttons.map( (b) => {
    //for mouse
    b.addEventListener("mousedown", buttonDown);
    b.addEventListener("mouseout", buttonUp);
    b.addEventListener("mouseup", buttonUp);

    //for touch
    b.addEventListener("touchstart", (e)=>{
        e.preventDefault();
        buttonDown(e);
    }, false);
    b.addEventListener("touchend", buttonUp);
    b.addEventListener("touchcancel", buttonUp);
});

//-------------------------------------------------------------------

//Setting
let gear = document.getElementById("setting");

let gearTouchStart = (e) => {
    //change gear speed 
    e.preventDefault();
    console.log("gear touch start");

    if(controller.EN)
        return ;

    console.log(e.type);
    if(e.type == "touchstart"){
        //navigator.vibrate(100);
        if(e.touches.length > 1){
            console.log("Multiple touch");
            return;
        }
    }
    
    console.log("set timeout");
    let timerID = window.setTimeout(()=>{
        console.log("show popup");
        //Show setting popup
        let p = document.getElementById("setting_popup");
        p.classList.add("is_show");
        gear.style.webanimationDuration = "20s";   
        console.log(gear.style.animationDuration);
        loadSettingFromESP32();    
    }, 1000);

    console.log("set cancel");
    let cancel = () => {
        console.log("popup show cancel")
        window.clearTimeout(timerID);
        gear.removeEventListener("mouseleave", cancel);
        gear.removeEventListener("touchend", cancel);
        gear.removeEventListener("touchmove", touchMove);
        gear.style.animationDuration = "20s";
    }
    gear.addEventListener("mouseleave", cancel);
    gear.addEventListener("mouseup", cancel);

    let touchMove = (e) => {
        e.preventDefault();
        let elm = document.elementFromPoint(e.touches[0].clientX, e.touches[0].clientY);
        //Leaved from gear ?
        if(elm != gear)
            cancel();
    };
    gear.addEventListener("touchmove", touchMove, passive = false);
    gear.addEventListener("touchend", cancel, passive = false);
    gear.style.animationDuration = "2s";
}
gear.addEventListener("mousedown", gearTouchStart, passive=false);
gear.addEventListener("touchstart", gearTouchStart,passive= false);
//

//Apply setting
let applySetting = () => {
    setting_json.baseName = document.getElementById("baseName").value;
    setting_json.WiFiSSID = document.getElementById("WiFiSSID").value;
    setting_json.WiFiPW = document.getElementById("WiFiPW").value;
    let stg = setting_json.preset[0];
    stg.Lbat = document.getElementById("logicBatteryType").value;
    stg.Mbat = document.getElementById("motorBatteryType").value;
    for(let i = 0; i < 4; i++){
        let reverse = document.getElementsByClassName("portReverse");
        stg.port[i].speed = sldr[i].Value * (reverse[i].checked ? -1.0 : 1.0);
        stg.port[i].assign = document.getElementsByClassName("portAssign")[i].value;
    }
    console.log(setting_json);
    uploadSettingFromBrowser(JSON.stringify(setting_json));
};
document.getElementById("setting_apply").onclick = applySetting;

//Popup close
let poupupClose = () => {
    //Hide setting popup
    let p = document.getElementById("setting_popup");
    p.classList.toggle("is_show");
};
document.getElementById("setting_close").addEventListener("click", poupupClose);

//OK button
document.getElementById("setting_ok").onclick = () => {
    applySetting();
    poupupClose();
};

//Read setting
let setting_json = {
    baseName:"A",
    settingNum: 0,//preset 0
    WiFiSSID:"",
    WiFiPW:"",
    preset:[
        {
            Lbat:"9.0 6.0",
            Mbat:"9.0 6.0",
            port:[
                {
                    speed: 100,
                    assign: "LEFT_WHEEL"
                },
                {
                    speed: 100,
                    assign: "RIGHT_WHEEL"
                },
                {
                    speed: 100,
                    assign: "BLUE_GREEN"
                },
                {
                    speed: 100,
                    assign: "RED_YELLOW"
                }
            ]
        }
    ]
};

let applySettingToUI = (setting)=>{
    console.log(setting);
    if(setting.baseName != null)
        document.getElementById("baseName").value = setting.baseName;
    if(setting.WiFiSSID != null)
        document.getElementById("WiFiSSID").value = setting.WiFiSSID;
    if(setting.WiFiPW != null)
        document.getElementById("WiFiPW").value = setting.WiFiPW;
    document.getElementById("motorBatteryType").value = setting.preset[0].Mbat;
    document.getElementById("logicBatteryType").value = setting.preset[0].Lbat;
    let reverse = document.getElementsByClassName("portReverse");
    let assign = document.getElementsByClassName("portAssign");
    let preset = setting.preset[0];
    for(let i = 0; i < 4; i++){
        //SPEED setting
        let speed = preset.port[i].speed;
        if(speed < 0.0){
            reverse[i].checked = true;
            sldr[i].Value = speed * (-1.0);
        }
        else{
            reverse[i].checked  = false;
            sldr[i].Value = speed;
        }
        
        //Port Assign setting
        assign[i].value = preset.port[i].assign;
    }
}

//Battery
class battery{
    constructor(elm_fig, elm_info){
        this.fig = elm_fig;
        this.info = elm_info;
    }
    set Value(v){
        let color;
        let v_fig = v;
        if(v >= 70)
            color = "green";
        else if(v >= 40)
            color = "#FFD700";
        else if(v >=20)
            color = "red";
        else{
            color = "red";
            v_fig = 20;
        }
        this.fig.style.background = "linear-gradient(to top, "+color+", "+color+" "+String(v_fig)+"%, white 10%)";
        this.info.textContent = String(v)+"%";
    }
}

let logicBattery = new battery(document.getElementById("logicbtrfig"), document.getElementById("logicbtrinfo"));
let motorBattery = new battery(document.getElementById("motorbtrfig"), document.getElementById("motorbtrinfo"));

window.onload = ()=> {
    loadSettingFromESP32();
}



//Slider
class slider{
    constructor(name, min, max, step, div_element){
        this.name = name;
        this.min = min;
        this.max = max;
        this.step = step;
        //var div_element = document.createElement("div");
        div_element.className = "TuneValue";
        div_element.innerHTML += '<input type="range" value ="0"id="'+name+'s" oninput="ChangeNumber(event)" min="'+min+'" max="'+max+'" step="'+step+'">';
        div_element.innerHTML+='<input type="number" id="'+name+'n" oninput="ChangeSlider(event)" min="'+min+'"max="'+max+'" step="'+step+'">';
        //var parent_object = document.getElementById("Slider");
        //parent_object.appendChild(div_element);
    }
    get Value(){
        return Number(document.getElementById(this.name+"s").value);
    }
    set Value(val){
        document.getElementById(this.name+"s").value=document.getElementById(this.name+"n").value=val;
    }
};

function ChangeSlider(event){
    document.getElementById(event.target.id.slice(0,-1)+"s").value=document.getElementById(event.target.id).value;
}
function ChangeNumber(event){
    document.getElementById(event.target.id.slice(0,-1)+"n").value=document.getElementById(event.target.id).value;
}

let sldr = [4];
sldr[0] = new slider("speed_A", 0, 100, 5, document.getElementById("slider_a"));
sldr[1] = new slider("speed_B", 0, 100, 5, document.getElementById("slider_b"));
sldr[2] = new slider("speed_C", 0, 100, 5, document.getElementById("slider_c"));
sldr[3] = new slider("speed_D", 0, 100, 5, document.getElementById("slider_d"));


//Firmware Update
document.getElementById("firmware").addEventListener("change", (e)=>{
    if(!window.confirm("Do not disconnect the LOGIC supply and network. \nDo you want to perform a firmware update?"))
        return;
    console.log(e.target.files[0]);
    let xhr = new XMLHttpRequest();
    let fd = new FormData();

    fd.append(e.target.files[0].name, e.target.files[0]);
    xhr.open("POST", "/update");
    xhr.upload.onloadstart = ()=>{
        document.getElementsByClassName("gear")[1].style.animationDuration = "2s";
        console.log("update start");
    };
    xhr.upload.onprogress = (ev)=>{
        let percent = (ev.loaded / ev.total * 100).toFixed(1);
        console.log(percent);
        document.getElementById("progress").textContent = "Firmware Update : " + String(percent) + "[%]...";
    };
    xhr.onreadystatechange = ()=>{
        if(xhr.status == 200){
            console.log("update end");
            document.getElementsByClassName("gear")[1].style.animationDuration = "20s";
            document.getElementById("progress").textContent = "Firmware Update : Complete.";
        }
    };
    xhr.send(fd);
});