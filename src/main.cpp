#include <Arduino.h>
#include <WiFi.h>
#include <Update.h>
#include "ESPAsyncWebServer.h"
//#include <SPIFFS.h>
#include <ESPmDNS.h>
#include "Ps3Controller.h"
#include "ESP-ROBOv2.h"
#include <EEPROM.h>
#include "ArduinoJson.h"
#include "WiFi-Setting.h"
#include "LITTLEFS.h"
#define CONFIG_LITTLEFS_SPIFFS_COMPAT 1
#define SPIFFS LITTLEFS
#include "setting.h"

ESP_ROBOv2 robo;
ESP_ROBOv2_Setting setting;
const IPAddress ip(192,168,30,1);
const IPAddress subnet(255,255,255,0);

char setting_str[512];
char hostname[16] = "esp-robov2";
char ssid[16] = "ESP-ROBOv2";
char WiFiSSID[32] = "";
char WiFiPW[32] = "";

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

void onConnectionLED(){
  for(int i = 0; i < 10; i++){
    digitalWrite(LED2, i%2);
    delay(100);
  }
}

void onWiFiEvent(WiFiEvent_t event)
{
  switch (event) {
 
    case SYSTEM_EVENT_STA_CONNECTED:
      Serial.printf("ESP32 Connected to WiFi Network.");
      Serial.println(WiFi.localIP());
      break;
    case SYSTEM_EVENT_AP_START:
      Serial.println("ESP32 soft AP started");
      Serial.println(WiFi.softAPIP());
      break;
    case SYSTEM_EVENT_AP_STACONNECTED:
      Serial.println("Station connected to ESP32 soft AP");
      break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
      Serial.println("Station disconnected from ESP32 soft AP");
      break;
    default: break;
  }
}
#define CONTROLLER_WS_CMD 0x01
void onWsEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len){
  switch(type){
    case WS_EVT_CONNECT:
      Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      break;
    case WS_EVT_DISCONNECT:
      Serial.printf("WebSocket client #%u disconnected\n", client->id());
      /*
      Serial.printf("Ps3 begin ");
      if(ws.count() == 0)
        Serial.println(Ps3.begin());
      */
      break;
    case WS_EVT_DATA:
      /*
      Serial.printf("WebSocket client #%u  DATA : ", client->id());
      for(int i = 0; i < len; i++)
        Serial.printf("%02X ", data[i]);
      */
      /*
      Data format

      | cmd | EN | x | y | TRIANGLE | RECTANGLE | CIRCLE | CROSS | 

      controller cmd = 0x01

      */
      if(data[0] == 0x01){
        Ps3.data.analog.stick.lx  = data[2];
        Ps3.data.analog.stick.ly  = data[3];
        Ps3.data.button.triangle  = data[4];
        Ps3.data.button.square    = data[5];
        Ps3.data.button.circle    = data[6];
        Ps3.data.button.cross     = data[7];
        if(data[1] == 0x01)
          robo.enableMD();
        else
          robo.disableMD();
      }
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
  }
  //Serial.printf("\n");
}

size_t content_len;
void handleUpdate(AsyncWebServerRequest *request, const String& filename, size_t index, uint8_t *data, size_t len, bool final){
  auto err = [&](){
      Update.printError(Serial);
      request->send(400);
  };

  if(!index){
    Serial.println("Update Start");
    content_len = request->contentLength();
    int cmd = (filename.indexOf("spiffs") > -1) ? U_SPIFFS : U_FLASH;
    //begin update
    if(!Update.begin(UPDATE_SIZE_UNKNOWN, cmd))
      err();
  }
  //write
  if(Update.write(data, len) != len)
    err();

  //final
  if(final){
    if(!Update.end(true))
      err();
    else{
      request->send(200);
      Serial.println("Update complete");
      Serial.flush();
      delay(100);
      ESP.restart();
    }
  }
}
//update progress
void printUpdateProgress(size_t prg, size_t sz){
  Serial.printf("Progress: %d%%\n", (prg*100)/content_len);
}

//Battery
void sendBatteryInfo(){
  uint8_t b[3] = {0x02, robo.logicBattery.getBatteryRemain(), robo.motorBattery.getBatteryRemain()};
  Serial.printf("LOGIC : %.2f[V] %d[%%]   MOTOR : %.2f[V] %d[%%]\n", robo.getLogicVoltage(), b[1], robo.getMotorVoltage(), b[2]);
  ws.binaryAll(b, 3);  
}

void setup() {
  //SPIFFS
  SPIFFS.begin();

  //
  Serial.begin(115200);  
  printf("hello\n");

  //ESP-ROBOv2
  robo.init();
  setting.loadSettingFromEEPROM();
  
  //WiFi
  WiFi.disconnect(true, true);
  WiFi.onEvent(onWiFiEvent);
  WiFi.mode((strlen(WiFiSSID)!=0) ? WIFI_MODE_APSTA : WIFI_MODE_AP);
  //WiFi.mode(WIFI_MODE_AP);
  WiFi.setTxPower(WIFI_POWER_MINUS_1dBm);
  WiFi.softAP(ssid);
  delay(200);
  WiFi.softAPConfig(ip, ip, subnet);
  delay(200);
  if(strlen(WiFiSSID) != 0){
    if(strlen(WiFiPW) != 0)
      WiFi.begin(WiFiSSID, WiFiPW);
    else
      WiFi.begin(WiFiSSID);
  }
  delay(200);

  //Web server
  server.on("/setting.json", HTTP_GET, [](AsyncWebServerRequest * request){
    request->send(200, "application/json", String(setting_str));
  });
  server.on("/setting_upload.json", HTTP_POST, 
    [](AsyncWebServerRequest *request) {
    Serial.println("setting upload");
    },
    NULL,
    [](AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) 
    {setting.onUploadSettingJson(request, data, len, index, total);}
    );
  
  server.on("/heap", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", String(ESP.getFreeHeap()));
  });

  server.on("/update", HTTP_POST, [](AsyncWebServerRequest *request){}, [](AsyncWebServerRequest *request, const String& filename, size_t index, uint8_t *data, size_t len, bool final){
    handleUpdate(request, filename, index, data, len, final);
  });
  Update.onProgress(printUpdateProgress);

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request){
    Serial.println("server on /");
    if(!Ps3.isConnected()){
      Serial.println("Ps3 end");
      Ps3.end();
      request->send(SPIFFS, "/index.html");
    }
    onConnectionLED();
  });
  server.serveStatic("/", SPIFFS, "/");
  
  ws.onEvent(onWsEvent);
  server.addHandler(&ws);
  server.begin();

  //MDNS
  if(!MDNS.begin(hostname))
    Serial.println("Error startting mDNS");
  MDNS.addService("http", "tcp", 80);

  /*
  Connection 
  HTTP_GET "/"" => Ps3 disable
  ws disconnected => Ps3 enable
  Ps3 connected => SoftAP disable
  Ps3 disconnected => SoftAP enable
  */
  //PS3
  Ps3.begin();
  Ps3.attachOnConnect([](){
    Serial.println("Ps3 connected");
    WiFi.softAPdisconnect(true);
    onConnectionLED();
  });
  Ps3.attachOnDisconnect([]{
    Serial.println("Ps3 disconnected");
    WiFi.softAP(ssid);
  });
  Serial.printf("BT MAC : %s\n", Ps3.getAddress().c_str());
}

unsigned long t = 0;
void loop() {
  bool active_flag = false;

  if(robo.MDisEnabled()){
    //wheel vx vy va
    robo.wheel->Move((double)Ps3.data.analog.stick.ly / 127.0, .0, (double)Ps3.data.analog.stick.lx / 127.0);
    const int8_t active_th = 10;
    if((abs(Ps3.data.analog.stick.ly) + abs(Ps3.data.analog.stick.lx)) > active_th)
      active_flag = true;

    //arm_1
    if(Ps3.data.button.triangle != 0){
      robo.arm_1->Move(1.0);
      active_flag = true;
    }
    else if(Ps3.data.button.square != 0){
      robo.arm_1->Move(-1.0);
      active_flag = true;
    }
    else
      robo.arm_1->Brake();
    

    //arm_2
    if(Ps3.data.button.circle != 0){
      robo.arm_2->Move(1.0);
      active_flag = true;
    }
    else if(Ps3.data.button.cross != 0){
      robo.arm_2->Move(-1.0);
      active_flag = true;
    }
    else{
      robo.arm_2->Brake();
    }
  }

  //LED1
  if((t % 2) == 0 && active_flag)
    digitalWrite(LED2, HIGH);
  else
    digitalWrite(LED2, LOW);
  
  //LED2
  if(robo.MDisEnabled())
    digitalWrite(LED1, HIGH);
  else{
    //Standby
    if(((t/20) % 2) == 0)
      digitalWrite(LED1, HIGH);
    else
      digitalWrite(LED1, LOW);
  }
  
  //Battery
  if((t % 200) == 0){
    sendBatteryInfo();
  }
  t++;
  delay(50);
}