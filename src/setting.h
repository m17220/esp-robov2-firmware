#ifndef __SETTING_H__
#define __SETTING_H__
#include <Arduino.h>
#include <EEPROM.h>
#include <ArduinoJson.h>
#include <ESPAsyncWebServer.h>
#include "ESP-ROBOv2.h"
#include "ROBO_MD.h"


extern char setting_str[512];

extern char hostname[16];
extern char ssid[16];
extern char WiFiSSID[32];
extern char WiFiPW[32];

class ESP_ROBOv2_Setting{
    public:
    StaticJsonDocument<1024> json;

    void loadSettingFromEEPROM();

    void writeDefaultSetting();

    //baseName setting
    void applyBaseNameSetting();

    //WiFi setting
    void applyWiFiSetting();

    //Battery setting
    void applyBatterySetting();

    void applySetting(char * s);

    void saveSetting();

    //Handle upload setting json from browser
    void onUploadSettingJson(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total);

};
#endif