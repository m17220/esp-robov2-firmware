#ifndef __ESP_ROBO_V2_H__
#define __ESP_ROBO_V2_H__
#include <Arduino.h>
//#include <SPIFFS.h>
#include <Wire.h>
//#include "Web.h"
#include "ROBO_MD.h"
#include "MoveBase.h"
#include "TwoWheel.h"
//Port List-------------------------------------------------------------------

#define LED1    21
#define LED2    19

#define P0A     35
#define P0B     32
#define P0C     23
#define P0D     22

#define P1A     33
#define P1B     25

#define P2A     26
#define P2B     27

#define P3A     17
#define P3B     16

#define P4A     18
#define P4B     5

#define VM_ADC  36
#define V_ADC   39

#define STBY    12
#define REF     26

#define R_SENSE 200E-3  //[Ω]
//-----------------------------------------------------------------------------

class ESP_ROBOv2{
    public:
    class Battery {
        public:

        float (*getVoltage)();

        void setMaxMinVoltage(float v_max, float v_min){
            maxVoltage = v_max;
            minVoltage = v_min;
        }

        //returns battery remain [%]
        uint8_t getBatteryRemain(){
            float v = getVoltage();
            float remain;
            remain = 100.0 * (v - minVoltage) / (maxVoltage - minVoltage);
            if(remain > 100.0)
                remain = 100.0;
            else if(remain < 0.0)
                remain = 1.0E-100;
            return (uint8_t)remain;
        }
        private:
        float maxVoltage;
        float minVoltage;
    };
    Battery logicBattery;
    Battery motorBattery;

    MD *wheel_md[4];
    MD *arm_1;
    MD *arm_2;

    MoveBase *wheel;
    enum md{
        A = 0,
        B,
        C,
        D
    };
    void init(){
        //I2C 
        Wire.begin(13, 15, 1000000);
        pinMode(13, PULLUP);
        pinMode(15, PULLUP);
        
        //MD 
        m[0] = new roboMD(0);
        m[1] = new roboMD(1);
        m[2] = new roboMD(2);
        m[3] = new roboMD(3);

        //STBY
        pinMode(STBY, OUTPUT);
        disableMD();
        //DAC
        setMdPeakCurrent(1.0);//1.0[A]

        //LED
        pinMode(LED1, OUTPUT);
        pinMode(LED2, OUTPUT);

        //ADC
        pinMode(VM_ADC, ANALOG);
        pinMode(V_ADC, ANALOG);
        analogSetAttenuation(ADC_11db);

        //Battery
        logicBattery.getVoltage = &getLogicVoltage;
        motorBattery.getVoltage = &getMotorVoltage;
    }

    void enableMD(){
        digitalWrite(STBY, 1);
        md_enabled = true;
    }

    void disableMD(){
        digitalWrite(STBY, 0);
        for(int i = 0; i <4; i++)
            m[i]->Brake();
        md_enabled = false;
    }

    bool MDisEnabled(){
        return md_enabled;
    }

    //I : md peak current[A]
    void setMdPeakCurrent(float I){
        float V_REF = I * R_SENSE;
        const float I_limit = 2.5;//[A]
        if(V_REF > (R_SENSE * I_limit))
            V_REF = R_SENSE * I_limit;
        dacWrite(REF, (uint8_t)(V_REF * 255.0 / 3.3));
    }

    static float getVoltage(uint8_t port){
        float v = ((float)analogRead(port));
        return  v * 3.9 * 5.5 / 4095.0;// VM * FullScale(3.7V) * 11dB / Resolution(12bit)
    }
    static float getMotorVoltage(){
        return getVoltage(VM_ADC);
    }

    static float getLogicVoltage(){
        return getVoltage(V_ADC);
    }

    void setMdRole(int id, const char * role){
        Serial.printf("%d %s\n", id, role);
        m[id]->Brake();
        if(strcmp(role, "LEFT_WHEEL") == 0)
            wheel_md[0] = m[id];
        else if(strcmp(role, "RIGHT_WHEEL") == 0)
            wheel_md[1] = m[id];
        else if(strcmp(role, "BLUE_GREEN") == 0)
            arm_1 = m[id];
        else if(strcmp(role, "RED_YELLOW") == 0)
            arm_2 = m[id];
    }

    void applyWheelMd(){
        if(wheel != _NULL)
            delete wheel;
        wheel = new TwoWheel(wheel_md);
    }
    
    MD *m[4];
    private:
    bool md_enabled;
};

extern ESP_ROBOv2 robo;

#endif
