#include "setting.h"

void ESP_ROBOv2_Setting::loadSettingFromEEPROM(){
  //EEPROM    //Adress 0x00~0x07 => data length
  EEPROM.begin(520);

  //Read Setting json length
  unsigned int settingDataLength = EEPROM.readUInt(0);
  EEPROM.end();
  if(settingDataLength > 512){
    Serial.println("invalid data length");
    writeDefaultSetting();
  }
  else{
    //Read setting json
    Serial.println("Read setting json");
    char s[512];
    EEPROM.begin(520);
    Serial.println(EEPROM.readBytes(8, s, settingDataLength));
    EEPROM.end();
    memcpy(setting_str, s, settingDataLength);
    applySetting(s);
  }
  
  //serializeJson(setting_json, setting_str);
  serializeJsonPretty(json, Serial);
  Serial.println(setting_str);
  //
}

void ESP_ROBOv2_Setting::writeDefaultSetting(){
  Serial.println("Write default setting");
  const char settingDefault[] = "{\"baseName\":\"\",\"settingNum\":0,\"WiFiSSID\":\"\",\"WiFiPW\":\"\",\"preset\":[{\"Lbat\":\"9.0 6.0\",\"Mbat\":\"9.0 6.0\",\"port\":[{\"speed\": 100,\"assign\": \"LEFT_WHEEL\"},{\"speed\": 100,\"assign\": \"RIGHT_WHEEL\"},{\"speed\": 100,\"assign\": \"BLUE_GREEN\"},{\"speed\": 100,\"assign\":\"RED_YELLOW\"}]}]}";
  EEPROM.begin(520);
  EEPROM.writeUInt(0, sizeof(settingDefault));
  EEPROM.writeBytes(8, settingDefault, sizeof(settingDefault));
  EEPROM.commit();
  EEPROM.end();
}

//baseName setting
void ESP_ROBOv2_Setting::applyBaseNameSetting(){
  Serial.println("Apply BaseName setting");
  if(!json["baseName"].isNull()){
    if(strlen((const char*)json["baseName"])==0)
      return;
    strcat(strcat(hostname, "-"), (const char*)json["baseName"]);
    strcat(strcat(ssid, "-"), (const char*)json["baseName"]);
  }
}

//WiFi setting
void ESP_ROBOv2_Setting::applyWiFiSetting(){
  Serial.println("Apply WiFi setting");
  if(!json["WiFiSSID"].isNull())
    strcpy(WiFiSSID, (const char*)json["WiFiSSID"]);
  if(!json["WiFiPW"].isNull())
    strcpy(WiFiPW, (const char*)json["WiFiPW"]);
}

//Battery setting
void ESP_ROBOv2_Setting::applyBatterySetting(){
  Serial.println("Apply Battery setting");
  const char * logicBattery = (const char*)json["preset"][json["settingNum"]]["Lbat"];
  const char *  motorBattery = (const char*)json["preset"][json["settingNum"]]["Mbat"];
  float max, min;
  sscanf(logicBattery, "%f %f", &max, &min);
  robo.logicBattery.setMaxMinVoltage(max, min);
  sscanf(motorBattery, "%f %f", &max, &min);
  robo.motorBattery.setMaxMinVoltage(max, min);
}

void ESP_ROBOv2_Setting::applySetting(char * s){
  Serial.println("applySetting");
  DeserializationError err = deserializeJson(json, s);

  //tests whether keys exists in the object
  const char *keys[] = {"baseName", "WiFiSSID", "WiFiPW", "preset"}; 
  bool containKeys = true;
  for(int i = 0; i < sizeof(keys)/sizeof(keys[0]); i++){
    if(json[keys[i]] == NULL){
      Serial.printf("Key [%s] is not exists\n", keys[i]);
      containKeys = false;
    }
  }

  if(err || !containKeys){  
    Serial.println(err.c_str());
    writeDefaultSetting();
  }
  else{
    serializeJson(json, setting_str);
  }
  applyBatterySetting();
  applyBaseNameSetting();
  applyWiFiSetting();

  for(int j = 0; j < 4; j++){
    //role assign
    char role[16];
    strcpy(role, (const char*)(json["preset"][0]["port"][j]["assign"]));
    Serial.println(role);
    robo.setMdRole(j, role);
    //speed
    ((roboMD*)(robo.m[j]))->speedRate = ((double)json["preset"][0]["port"][j]["speed"]) / 100.0;
    Serial.println(((roboMD*)(robo.m[j]))->speedRate);
  }
  robo.applyWheelMd();
}

void ESP_ROBOv2_Setting::saveSetting(){
  Serial.println("saveSetting");
  uint32_t len = strlen(setting_str);
  EEPROM.begin(520);
  EEPROM.writeUInt(0, len);
  EEPROM.writeBytes(8, setting_str, len);
  EEPROM.commit();
  EEPROM.end();
}

//Handle upload setting json from browser
void ESP_ROBOv2_Setting::onUploadSettingJson(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total){
  Serial.println("onUpload setting json");
  static char s[512];
  request->send(200);
  memcpy(s + index, data, len);

  if((index+len) == total){
    //Upload End
    applySetting(s);
    saveSetting();
    Serial.println(setting_str);
  }
}
